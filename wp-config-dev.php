<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_mehul_portfolio');

/** MySQL database username */
define('DB_USER', 'wp_mehul');

/** MySQL database password */
define('DB_PASSWORD', 'Koviko#14473');

/** MySQL hostname */
//For local environment & connect 'ssh -L 3310:127.0.0.1:3306 k9taut5q327s@43.255.154.106' on Mac os
define('DB_HOST', '127.0.0.1:3310');
//For Server on Go Daddy
//define('DB_HOST', 'localhost:3306');


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** This will Change static URL to Dynamic URL. */
define('WP_SITEURL', 'http://mehulchopda.local/mehul_portfolio');
define('WP_HOME', 'http://mehulchopda.local/mehul_portfolio');

/** This will upload all your media files under the wp-content/files. */
define( 'UPLOADS', 'wp-content/'.'files' );

/**
 * Other customizations.
 */
//define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
//define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
//define('AUTOMATIC_UPDATER_DISABLED', true);

//only admin can modify the forms
//define('WPCF7_ADMIN_READ_CAPABILITY', 'manage_options');
//define('WPCF7_ADMIN_READ_WRITE_CAPABILITY', 'manage_options');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w^48,r|/c-~Bsf8(2RRvKK)$FaK@eX2D1sx5{! Y#du[X;u<3%eXb=PpmK`L~*5&');
define('SECURE_AUTH_KEY',  '=*0XRl+f+pTvfiHw|+wOrCg2(&;]a]IFtORn8qf},|,/hw48 z601xF(+MEB9Ak;');
define('LOGGED_IN_KEY',    'ekCl^l#TiB=iB<F9B+Zn:CCQbm%L3`mRN.@>XM|<]Z?6US eem{?+8toP%9>H73?');
define('NONCE_KEY',        '46R[Qhw@#aVz-kuK9?Xe-FpL)Tc+d[-`iOz*j8N8Dsby-3S6ucdI Y=|MhZG~H;2');
define('AUTH_SALT',        'mO33b12plV>Hs-Lnx2/=h8h:qdR,C{>eZX}NPhZ.BqWJv*#gcaVM|n&)^hwaG4lp');
define('SECURE_AUTH_SALT', 'aj;!3o+LUS{tp_U`=<k19cQFj>[&-*R3O1rwNv,VI( *GpIhbUmQsF`K~P2W2=.&');
define('LOGGED_IN_SALT',   'jxF}~_SSA6m~8D(mt;Ozg-M.R.+6IVLv9~Q8rcUbQK{ssZEU:4jdVd+ywrW*+ax~');
define('NONCE_SALT',       'si[x?GHsnzKu(sg.t?cL2y%X|2A!0D1=pXkF+fo[e>NUt>3Jh^/~_~V}*S2Yo)]W');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/** This will enable the debugging. */
define( 'WP_DEBUG', True );
//define( 'FORCE_SSL_ADMIN', True );
define( 'SAVEQUERIES', False );
/** This will enable site origin debugging. */
//define('SITEORIGIN_PANELS_DEV', true);
//define('WP_ALLOW_REPAIR', true);
/* That's all, stop editing! Happy blogging. */
/** Enable W3 Total Cache */
//define('WP_CACHE', true); // Added by W3 Total Cache

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


////wp-super-cache plugin
//require_once(ABSPATH . 'wp-settings.php');